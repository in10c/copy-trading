const { secretDeveloperKey } = require("./constants");

    const verifyDeveloperKey = (req, res, next) => {
        const devKey = req.headers["dev-key"];
        if (devKey && devKey === secretDeveloperKey) {
            next();
        } else {
            res.send({ success : 0, message: "Lo sentimos, no tienes acceso aqui" });
        }
    };


module.exports = verifyDeveloperKey;