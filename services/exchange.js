const Binance = require('../forks/binance-api-node').default
const BinanceDos = require('node-binance-api');
const ProtectedBalance = require("../models/ProtectedBalance")
const Api = require("../models/Api")
const Constants = require("../services/constants")
const {fixedToPattern} = require("../services/numbers")
const Slot = require("../models/Slot")

class Exchange {
    async getClient(userID){
        let credentials = await Api.findOne({userID})
        if(!credentials) throw "No se encuentran API´s de Binance registradas en la base de datos"
        return Binance({
            apiKey: credentials.apiKey,
            apiSecret: credentials.apiSecret
        })
    }
    async getAlternativeClient(userID){
        let credentials = await Api.findOne({userID})
        if(!credentials) throw "No se encuentran API´s de Binance registradas en la base de datos"
        return new BinanceDos().options({
            APIKEY: credentials.apiKey,
            APISECRET: credentials.apiSecret
        });
    }
    async getAnonClient(){
        return Binance()
    }
    //funciones auxiliares
    //integrar balances protegidos
    async getCoinBalance(userID, coin, _client){
        try {
            //si no enviamos conexion genera una nueva
            const client = _client ? _client : await this.getClient(userID)
            let accntInf = await client.accountInfo({'recvWindow': 50000})
            let filt = accntInf.balances.filter(val=> val.asset === coin)
            console.log("traigo para ver el total ")
            return {success: 1, balance: filt[0].free, locked: filt[0].locked}
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }   
    //inclluir despues el porcentaje del balance total
    async getBuyBalance(userID, symbol, lotSize, binanceClient) {
        let {success, balance, message} = await this.getCoinBalance(userID, "BTC", binanceClient)
        if(success === 0 ) throw message
        let stats = await binanceClient.dailyStats({ symbol })
        //si es compra se divide por el precio de compra
        let balancePortion = balance * Constants.enterPercentage
        let quantityToBuy = fixedToPattern(balancePortion / stats.askPrice, lotSize)
        return quantityToBuy
    }
    async createBuyOrder(userID, symbol, price, lotSize){
        try {
            const binanceClient = await this.getClient(userID)
            let buyBalance = await this.getBuyBalance(userID, symbol, lotSize, binanceClient)
            //console.log("el buy balance a usar es ", buyBalance)
            let responseBuyOrder = await binanceClient.order({ symbol, side: "BUY", quantity: buyBalance, price })    
            console.log("la respuesta de buy order", responseBuyOrder)
            return { success: 1, numericOrderID: responseBuyOrder.orderId }
        } catch (error) {
            //console.log("ocurrio un error", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async deleteOCO( userID, symbol, sellOrderID ){
        try {
            const binanceClient = await this.getClient(userID)
            let response = await binanceClient.cancelOrder({ symbol, origClientOrderId: sellOrderID })
            console.log("response de cancelar oco", response)
            return {success: 1}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async newOCO(userID, tradedPair, lotSize, symbol, _sl, _gain, beforeQuantity){
        try {
            const binanceClient = await this.getClient(userID)
            //traemos nuevo balance
            if(beforeQuantity){
                console.log("entro a newoco con quant", beforeQuantity)
                let respNewOCO = await binanceClient.orderOco({ symbol, side:  "SELL", quantity: beforeQuantity, price: _gain, stopPrice: _sl, stopLimitPrice: _sl })
                console.log("trajo de nueva OCO", respNewOCO)
                return { success: 1, orderID: respNewOCO.orders[0].clientOrderId }
            } else {
                const { success, balance } = await this.getCoinBalance(userID, tradedPair, binanceClient)
                if ( success === 0 ) throw "No se ha podido obtener el balance"
                console.log("la cantidad a vender es ", balance)
                const quantityToSell = fixedToPattern(balance, lotSize)
                //asignamos balance a orden
                let respNewOCO = await binanceClient.orderOco({ symbol, side:  "SELL", quantity: quantityToSell, price: _gain, stopPrice: _sl, stopLimitPrice: _sl })
                console.log("la respuesta de la nueva oco", respNewOCO)
                return { success: 1, orderID: respNewOCO.orders[0].clientOrderId, price: respNewOCO.orderReports[0].price, qty: respNewOCO.orderReports[0].origQty }
            }
        } catch (error) {
            console.log("hub", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async getOrder(orderID, userID, symbol){
        try {
            const binanceClient = await this.getClient(userID)
            let order = await binanceClient.getOrder({ symbol, origClientOrderId: orderID })    
            return {success: 1, order}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async editBuyOrder(orderID, userID, symbol, quantity, enterprice, lotSize){
        try {
            const binanceClient = await this.getClient(userID)
            let response = await binanceClient.cancelOrder({ symbol, origClientOrderId: orderID })
            let buyBalance = fixedToPattern(quantity, lotSize)
            let cleanPrice = this.convertExponentialToDecimal(enterprice)
            console.log("repsuesta de eliminar buy order", response, { symbol, side: "BUY", quantity, enterprice })
            let order = await binanceClient.order({ symbol, side: "BUY", quantity: buyBalance, price: cleanPrice })  
            console.log("repsuesta de crear buy order", order)
            return {success: 1, order}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async panicSell(userID, inPair, lotSize, symbol, MIN_NOTIONAL){
        try {
            console.log("nuevo panic sell", userID, inPair, lotSize, symbol)
            const binanceClient = await this.getClient(userID)
            let respBalance = await this.getCoinBalance(userID, inPair, binanceClient)
            if(respBalance.success === 0 ) throw respBalance.message
            let protectedAltcoin = await this.getProtected(userID, inPair)
            console.log("el balance protegido es ", protectedAltcoin)
            let quantityToSell = parseFloat(respBalance.balance) - parseFloat(protectedAltcoin)
            console.log("de valace", quantityToSell)
            quantityToSell = fixedToPattern(quantityToSell, lotSize)
            console.log("de vaquantityToSelllace", quantityToSell)
            if(quantityToSell > MIN_NOTIONAL){
                let response = await binanceClient.order({
                    type: "MARKET", symbol, side: "SELL", quantity: quantityToSell,
                })
                console.log("respuesta de mercado", response)
                return {success: 1}
            } else {
                //regreso bien, no se puede vender porque es polvo
                return { success: 1 }
            }
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
        
    }
    async getAnalyticsData(userID, selectedPackage, balance, allOpenOrders) {
        
        console.log("traigo todos los balances", allOpenOrders)
        let yetinorder = 0
        allOpenOrders.map(orderQueued=>{
            yetinorder += parseFloat(orderQueued.pretendedToBuy)
        })
        //let notosell = await this.getProtected(userID, "BTC")
        console.log("lo que ya esta en orden", yetinorder, balance)
        let newBalance = parseFloat(balance) //- notosell 
        // 0 - ver si aun tiene saldo de trade
        if(newBalance < Constants.minTrade) return {success: 0, message: "El usuario no tiene saldo en btc para hacer trading." }
        // 1 - definir balance portion
        let auxTBWO = parseFloat(newBalance) + parseFloat(yetinorder)
        let totalBalanceWithOrders = ( auxTBWO > Constants.packgs[selectedPackage].limits ) ? Constants.packgs[selectedPackage].limits : auxTBWO
        let percentageToUse = totalBalanceWithOrders < 0.015 ? Constants.bronzeEnterPercentage : Constants.enterPercentage
        let auxBalancePortion = totalBalanceWithOrders * percentageToUse
        // 2 - que balance usar
        let balancePortion = auxBalancePortion > newBalance ? newBalance : auxBalancePortion
        let usedBTC = balancePortion + yetinorder
        // 3 - revisar limites de licencia 
        if(usedBTC > Constants.packgs[selectedPackage].limits ){
            return {success: 0, message: "El usuario ha alcanzado los límites de su licencia" }   
        } else {
            return {success: 1, yetinorder, balancePortion, totalBalanceWithOrders : usedBTC, limits: Constants.packgs[selectedPackage].limits, percentageToUse }
        }
        // if(balancePortion > newBalance) {
        //     console.log("el balance es ", balancePortion, newBalance, userID)
        //     //ahora sumanmos con btc 
        //     let usedBTC = newBalance + yetinorder
        //     if( usedBTC > Constants.packgs[selectedPackage].limits ) return {success: 0, message: "El usuario ha alcanzado los límites de su licencia" }
        //     return {success: 1, yetinorder, balancePortion: newBalance, totalBalanceWithOrders, limits: Constants.packgs[selectedPackage].limits }    
        // } else {
        //     console.log("else ...", balancePortion, newBalance, userID)
        //     let usedBTC = balancePortion + yetinorder
        //     //si el usuario llega a sus limites de licencia con la porcion
        //     if( usedBTC > Constants.packgs[selectedPackage].limits ){
        //         let usedMin = newBalance + yetinorder
        //         if( usedMin > Constants.packgs[selectedPackage].limits ){
        //             return {success: 1, yetinorder, balancePortion: usedMin, totalBalanceWithOrders, limits: Constants.packgs[selectedPackage].limits }    
        //         } else {
        //             return {success: 0, message: "El usuario ha alcanzado los límites de su licencia" }    
        //         }
        //     //si todo esta bien regresamos el saldo
        //     } else {
        //         return {success: 1, yetinorder, balancePortion, totalBalanceWithOrders, limits: Constants.packgs[selectedPackage].limits }
        //     }
        // }
        
    }
    async getProtected(userID, coin){
        let protectedAmounts = await ProtectedBalance.findOne({userID})
        if(!protectedAmounts) return 0
        //console.log("si tiene protected de las moneda ", protectedAmounts)
        const { balances } = protectedAmounts
        let matchs = balances.filter(val=> val.asset === coin)
        if(matchs.length === 0 ) return 0
        return parseFloat(matchs[0].quantity)
    }
    async getAllBalances(userID, _client){
        try {
            //si no enviamos conexion genera una nueva
            const client = _client ? _client : await this.getClient(userID)
            let accntInf = await client.accountInfo({'recvWindow': 50000})
            return {success: 1, allBalances: accntInf.balances}
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }

    async getLotSize(asset) {
        try {
            const symbol = asset === "USDT" ? "BTC"+asset :  asset+"BTC" 
            const binanceClient = await this.getAnonClient()
            const infoExch = await binanceClient.exchangeInfo()
            const symbolData = infoExch.symbols.filter(val=> val.symbol === symbol)[0]
            console.log("infoExch.symbols", symbolData)
            const lotSize = symbolData.filters.filter(val=> val.filterType==="LOT_SIZE")[0]
            const MIN_NOTIONAL = symbolData.filters.filter(val=> val.filterType==="MIN_NOTIONAL")[0]
            console.log("el min not", MIN_NOTIONAL)
            return {success: 1, lotSize, MIN_NOTIONAL}
        } catch (error) {
            console.log("error al buscar lotsize", error)
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async buyAsset( userID, asset, quantity, buyType, stepSize ) {
        try {
            const symbol = asset === "USDT" ? "BTC"+asset :  asset+"BTC" 
            const binanceClient = await this.getClient(userID)
            //comparamos el tipo de compra 
            if(buyType === "quantityBuy"){
                const quantityToBuy = fixedToPattern(quantity, stepSize)
                console.log("respuesta de comprar", quantity, quantityToBuy, stepSize)
                let response = await binanceClient.order({
                    type: "MARKET", symbol, side: "BUY", quantity: quantityToBuy,
                })
                console.log("resp de compra", response)
                return {success: 1}
            //para una compra por porcentage
            } else {
                let respBalance = await this.getCoinBalance(userID, "BTC", binanceClient)
                if(respBalance.success === 0 ) throw respBalance.message
                let quantityToSell = respBalance.balance
                console.log("de valace", quantityToSell)
                let percentage = quantityToSell * ( quantity / 100 )
                const quantityToBuy = fixedToPattern(percentage, stepSize)
                console.log("respuesta de comprar por percentage", percentage, quantityToBuy, stepSize)
                let response = await binanceClient.order({
                    type: "MARKET", symbol, side: "BUY", quantity: quantityToBuy,
                })
                console.log("resp de compra", response)
                return {success: 1}
            }
        } catch (error) {
            console.log("error al de comprar", error)
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async getAskPrice(asset) {
        try {
            const symbol = asset === "USDT" ? "BTC"+asset :  asset+"BTC" 
            const binanceClient = await this.getAnonClient()
            const { askPrice } = await binanceClient.dailyStats({ symbol })
            return {success: 1, askPrice}
        } catch (error) {
            console.log("error al traer precio", error)
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async getExchangeOrders(userID, _client){
        try {
            //si no enviamos conexion genera una nueva
            const client = _client ? _client : await this.getAlternativeClient(userID)
            const openOrders = await client.openOrders(false)
            //console.log("las ordenes abiertas son", openOrders)
            return {success: 1, openOrders}
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async getAllProtected(userID){
        let protectedAmounts = await ProtectedBalance.findOne({userID})
        return protectedAmounts
    }
    async marketSell(quantity, asset, userID, lotSize){
        const symbol = asset+"BTC"
        //console.log("en market sell", quantity, asset, userID, lotSize, symbol)
        const binanceClient = await this.getClient(userID)
        try {
            let quantityToBuy = fixedToPattern(quantity, parseFloat(lotSize.stepSize))
            if(quantityToBuy <= 0 ) return {success: 0, message: "Nada para vender o polvo."}
            if(quantityToBuy < parseFloat(lotSize.minQty) ) return {success: 0, message: "La cantidad que se pretende vender es menor a la permitida por el exchange."}
            console.log("a vender", quantityToBuy)
            let response = await binanceClient.order({
                type: "MARKET", symbol, side: "SELL", quantity: quantityToBuy,
            })
            console.log("reso de venta mercado", response)
            return {success: 1}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async getSlot(userID){
        return await Slot.findOne({ userID })
    }
    async symbolOpenOrders(userID, symbol){
        try {
            const binanceClient = await this.getClient(userID)
            let response = await binanceClient.openOrders({ symbol })
            let buyOrdrs = response.filter(val=> val.side === "BUY") 
            let sellOrdrs = response.filter(val=> val.side === "SELL") 
            return {success: 1, numberOfBuyOrders: buyOrdrs.length, numberOfSellOrders: sellOrdrs.length}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async symbolFiltrOpenOrders(userID, symbol, side){
        try {
            const binanceClient = await this.getClient(userID)
            let response = await binanceClient.openOrders({ symbol })
            //console.log("respuesta de ordenes abiertas del symbol", response)
            let filtrdOrdrs = side ? response.filter(val=> val.side === side) : response
            //console.log("respuesta de filtradas", filtrdOrdrs)
            return {success: 1, numberOfOpenOrders: filtrdOrdrs.length, filtrdOrdrs}
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async sellDust(userID){
        try {
            const binanceClient = await this.getClient(userID)
            binanceClient.obtenerLogDUST((error, dustlog) => {
                console.log("hay err", error)
                console.log("de vender polgho")
                console.log(dustlog);
            })
        } catch (error) {
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    async regenerateQuantities(account, inPair, initialOrderID){
        try {
            const symbol = inPair+'BTC'
            const binanceClient = await this.getClient(account)
            // let trades = await binanceClient.myTrades({
            //     symbol: inPair+'BTC'
            // })
            let buyOrders = []
            let sellOrders = []
            let neutralOrders = []
            let totalBuyed = 0
            let totalSelled = 0
            let orddrs = await binanceClient.allOrders({
                symbol,
                orderId: initialOrderID,
                recvWindow: 50000
            })
            console.log("los ytaes son", orddrs)
            let initialTime = null
            for (let index = 0; index < orddrs.length; index++) {
                const element = orddrs[index];
                if(element.orderId){
                    initialTime = element.time
                }
                if(element.cummulativeQuoteQty && !isNaN(element.cummulativeQuoteQty) && element.cummulativeQuoteQty > 0 ){
                    if(element.side === "BUY"){
                        totalBuyed += element.cummulativeQuoteQty
                        buyOrders.push(element)
                    } else {
                        totalSelled += element.cummulativeQuoteQty
                        sellOrders.push(element)
                    }
                } else {
                    neutralOrders.push(element)
                }
            }
            console.log("buyOrders", buyOrders, "sellOrders", sellOrders, "neutralOrders", neutralOrders, totalBuyed, totalSelled)
            let change = totalSelled - totalBuyed
            let percentChage = ( change * 100 ) / totalBuyed
            console.log("calculos", change, percentChage)
            // let trades = await binanceClient.myTrades({
            //     symbol, recvWindow: 50000, limit: 500
            // })
            // console.log("saco todos los trades", trades)

            // for (let idxBuy = 0; idxBuy < buyOrders.length; idxBuy++) {   
            //     let item = buyOrders[idxBuy]    
            //     let filtered = trades.filter(val=>val.orderId === item.orderId)
            //     console.log("los trades de la orden ", item.orderId, filtered)
            //     buyTXs = buyTXs.concat(filtered)
            // }

            // for (let idxSell = 0; idxSell < sellOrders.length; idxSell++) {     
            //     let item = sellOrders[idxSell]               
            //     let filtered = trades.filter(val=>val.orderId === item.orderId)
            //     console.log("los trades de la orden ", item.orderId, filtered)
            //     sellTXs = sellTXs.concat(filtered)
            // }
            // console.log("terminamos ordenar las txs", buyTXs, sellTXs)
            return {success: 1, buyOrders, sellOrders, neutralOrders, totalBuyed, totalSelled , change : change.toFixed(8), percentChage: percentChage.toFixed(2) }
        } catch (error) {
            console.log("el error fue", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }

    async marketBuy( userID, asset, quantity  ) {
        try {
            const symbol = asset+"BTC" 
            const binanceClient = await this.getClient(userID)
            console.log("acomprar ", quantity)
            let response = await binanceClient.order({
                type: "MARKET", symbol, side: "BUY", quantity,
            })
            console.log("resp de compra", response)
            return {success: 1}
            
        } catch (error) {
            console.log("error al de comprar", error)
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async cancelOrders(userID, symbol, respOpenOrders){
        try {
            const binanceClient = await this.getClient(userID)
            console.log("vamosa cancelar ", userID, symbol, respOpenOrders)
            for (let index = 0; index < respOpenOrders.length; index++) {
                const {clientOrderId, type} = respOpenOrders[index];
                if(type != "STOP_LOSS_LIMIT"){
                    console.log("si pasa a cancelar")
                    await binanceClient.cancelOrder({
                        symbol, origClientOrderId: clientOrderId
                    })
                }
            }
            return {success: 1}
        } catch (error) {
            console.log("hubo un error al cancelar orden", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    
    //v3 copy-trading
    async createServerBuyOrder(userID, btcPortion, symbol, price, stepSize){
        try {
            const binanceClient = await this.getClient(userID)
            let buyBalance = fixedToPattern(parseFloat(btcPortion) / parseFloat(price), stepSize)
            let cleanPrice = this.convertExponentialToDecimal(price)
            let responseBuyOrder = await binanceClient.order({ symbol, side: "BUY", quantity: buyBalance, price : cleanPrice })    
            console.log("la respuesta de buy order", responseBuyOrder)
            return { success: 1, orderID: responseBuyOrder.clientOrderId, newOrder: responseBuyOrder, buyBalance }
        } catch (error) {
            //console.log("ocurrio un error", error)
            return { success: 0, message: error.message ? error.message : error }
        }
    }
    convertExponentialToDecimal(exponentialNumber) {
        // sanity check - is it exponential number
        const str = exponentialNumber.toString();
        if (str.indexOf('e') !== -1) {
            const result = exponentialNumber.toFixed(8);
            return result;
        } else {
            return exponentialNumber;
        }
    }
}

module.exports = new Exchange()