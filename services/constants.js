const bitcoin = require('bitcoinjs-lib')

const Constants = {
    production: false,
    network: "mainnet",
    enterPercentage: .08,
    bronzeEnterPercentage: .15,
    minTrade: 0.0005,
    statusActivations: false,
    secretKey: "X$dfRb5Uy7$gdE5HrkDa!*lFg+by~RfT",
    secretDeveloperKey: "#?xW47q7-7p=hkzrc&ya#W$#hvM_hjN4uz2LcU_4g+*HA&eLjJV29yD%wxTayxs2",
    packgs : {
        1: {name:"Bronce", limits: 0.042 }, 
        2: {name: "Plata", limits: 0.084 }, 
        3: {name:"Oro", limits: 0.212 }, 
        4: {name:"Diamante", limits: 0.509 }, 
        5: {name:"Premium", limits: 1.018 }, 
        6: {name:"Premium Plus", limits: 2.036 }
    },
    getBlockchainFee : () =>{
        return Constants.production ? 10000 : 600
    },    
    databaseStringConn: ()=>{
        if(Constants.production){
            return 'mongodb://mongoAdmin:Vudh8Li1rzxPxQ7DnkMLS33@82.223.104.116:27017/bitwabi?authSource=admin'
        } else {
            return 'mongodb://localhost:27017/bitwabiprod'
        }
    },
    appUrl: ()=>{
        if(Constants.production){
            return 'https://app.bitwabi.com'
        } else {
            return 'http://localhost:3000'
        }
    },
    btcExplorer: ()=>{
        if(Constants.network === "mainnet"){
            return 'https://blockstream.info/tx'
        } else {
            return 'https://blockstream.info/testnet/tx'
        }
    },
    pckgInfo : (packNumber) =>{
        return Constants.packgs[packNumber]
    },
    tradersIDs:[
        "5f38868a0a71072c2089354e",
        "5f5021b8c5279f3fc41d8685",
        "5f590fe0ef669fde4fbddda6"
    ],
    walletYhon :()=> Constants.network === "mainnet" ? "17QUgAomaF6mttzwowqLi786iZTdwsS9un" : "mnfQSfPHef9E48QtMWRtRhgMQ3KuXs4osg",
    walletAlex :()=> Constants.network === "mainnet" ? "bc1qzqwljxh680rf603fdfgkrjwkltyzexe53w07ld" : "tb1qvjw0tyr6rc7hz400nkx4tl2es3qrqn3v8mfywd",
    walletOscar : ()=>Constants.network === "mainnet" ? "34G5hsmbzWPUavYtjSinDhkMRCs5AmBiPz" : "tb1qthkufzaxdfvymw6hjpd5w5eva5jlqx6ygjma7g",
    walletCristian :()=> Constants.network === "mainnet" ? "3KupJ9c13xngBxmjNLgECNCY7sYt7yuTbj" : "mznGJRvZaDZ3HvDZe1RyExQiNLwUaj6KFT",
    walletDiego :()=> Constants.network === "mainnet" ? "3DS3gaBzSqVRKTUXQHHMfTaKAoTsQMUyUe" : "tb1qnq3z8e5zg9hptq9z8ngdh0eew84jlms5preenm",
    walletCommunitaria : () => Constants.network === "mainnet" ? "38Rk5eGgSDUEQc3acUgwr5URVJmexAuQyP" : "tb1qvhuysvnxmlqh3z8wnecrsngd2wfvnnm97rcf8k",
    globalNetwork :()=> Constants.network === "mainnet" ? bitcoin.networks.bitcoin : bitcoin.networks.testnet,
    blockstream :()=> Constants.network === "mainnet" ? "https://blockstream.info" : "https://blockstream.info/testnet"

};

module.exports = Constants