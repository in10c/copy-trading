function fixedToPattern(number, pattern){
    let decimals= countDecimals(pattern)
    let multiplierString = "1"
    for (let index = 1; index <= decimals; index++) {
        multiplierString += "0"
    }
    let multiplier = parseInt(multiplierString)
    console.log("el multiplier es ", multiplier, number)
    return Math.floor('' + (number * multiplier)) / multiplier;
}

function fixedToPatternRounded(number, pattern){
    let decimals= countDecimals(pattern)
    let multiplierString = "1"
    for (let index = 1; index <= decimals; index++) {
        multiplierString += "0"
    }
    let multiplier = parseInt(multiplierString)
    console.log("el multiplier es ", multiplier, number)
    return Math.ceil('' + (number * multiplier)) / multiplier;
}

function fixedToMultiplier(number, multiplier){
    return Math.floor('' + (number * multiplier)) / multiplier;
}

function getMultiplier(pattern){
    //console.log("llego el pattern: ", pattern)
    let decimals= countDecimals(pattern)
    let multiplierString = "1"
    for (let index = 1; index <= decimals; index++) {
        multiplierString += "0"
    }
    let multiplier = Math.floor(multiplierString)
    return multiplier
}

function countDecimals(value){
    if(Math.floor(value) === value) return 0;
    return value.toString().split(".")[1].length || 0; 
}

module.exports = {fixedToPattern, getMultiplier, fixedToMultiplier, countDecimals, fixedToPatternRounded}