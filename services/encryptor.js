const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = "LPHnLKz{T@T;#>Y<Sc=j^iDno9ao_hkz";
const iv = "r6.1Sz~N%nf06.,&";

function encrypt(text) {
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return encrypted.toString('hex')
}

function decrypt(text) {
    //let iv = Buffer.from(text.iv, 'hex');
    let encryptedText = Buffer.from(text, 'hex');
    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

exports.encrypt = encrypt
exports.decrypt = decrypt