const Sockets = require("../controllers/Sockets")

async function createSloty(data){
    const { userID, socketID, clusterNumber, reBorn} = data 
    await Sockets.createSlot(userID, socketID, clusterNumber, reBorn)
}

exports = module.exports = function(io){
    io.on('connection', (socket) => {
        console.log('un usuario se conecto al socket del servidor', socket.id);
        //to join alerts room (turn on bot)
        socket.on("saveSlot", data=> {
            createSloty(data)
            console.log("se anade al room ")
            socket.join(data.userID)
        })
        //warning on disconnect
        socket.on('disconnect', (reason) => {
            console.log('a user has disconnected to alerts', reason);
        });
    })
}