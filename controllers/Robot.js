const Exchange = require("../services/exchange")
const OrdersQueue = require("../models/OrdersQueue")
const Order = require("../models/Order")
const Signal = require("../models/Signal")

class Robot {

    async createBuyOrder(userID, orderID, symbol, price, lotSize){
        console.log("nao dn ereacrobot ")
        let response = await Exchange.createBuyOrder(userID, symbol, price, lotSize)
        if(response.success===0){
            console.log("vamos a actualizar order queue", orderID)
            await this.changeOrderStatus(orderID, "Error al crear la orden de compra: "+message)
        } else {
            await this.changeOrderStatus(orderID, "Éxito al crear la orden de compra")
        }
    }
    async getBinanceClient(userID){
        let exchangeClient = await Exchange.getClient(userID)
        return exchangeClient
    }
    async changeOrderStatus(orderID, newStatus){
        let today = new Date()
        let currentOrder = await OrdersQueue.findById(orderID)
        currentOrder.globalStatus = newStatus
        if(!currentOrder.logs){
            currentOrder.logs = []
        }
        currentOrder.logs.push({
            description: newStatus,
            time: today.getTime()
        })
        await currentOrder.save()
    }
    async updateOrder(orderID, data){
        await OrdersQueue.findByIdAndUpdate(orderID, data)
    }
    async editOCO(userID, orderID, sl, gain){
        const { symbol, lotSize, tradedPair, sellOrderID, sellOrderQuantity } = await OrdersQueue.findById(orderID)
        //si hay una orden de venta, que se puede modificar
        console.log("como ya tenia una antes eliminamos")
        let respon = await Exchange.deleteOCO(userID, symbol, sellOrderID)
        if(respon.success === 0) return respon
        console.log("camos a crear nueva orden")
        let response = await Exchange.newOCO(userID, tradedPair, lotSize, symbol, sl, gain, sellOrderQuantity)
        await this.changeOrderStatus(orderID, response.success===0 ? "Error al editar la orden de venta: "+response.message: "Éxito al editar la orden de venta")
        if(response.success===1){
            await OrdersQueue.findByIdAndUpdate(orderID, { sl, gain })
        }
        return response
    }
    async createOCO(userID, orderID){
        const { sl, gain, symbol, lotSize, tradedPair } = await OrdersQueue.findById(orderID)
        let response = await Exchange.newOCO(userID, tradedPair, lotSize, symbol, sl, gain)
        await this.changeOrderStatus(orderID, response.success===0 ? "Error al crear la orden de venta: "+response.message: "Éxito al crear la orden de venta")
        return response
        
    }
    async updateOrderRegistry(orderID, newStatus){
        console.log("vamos a cambiar update a ", orderID, newStatus)
        await this.changeOrderStatus(orderID, newStatus)
        return newStatus
    }
    async updateBuyOrderStatus(orderID, msg, totalBuyed){
        //reaccionamos dependiendo el estado
        if(msg.orderStatus === "NEW"){
            //si es nueva solo actuyalizamos estado
            await this.updateOrder(orderID, {
                buyOrderID: msg.newClientOrderId, buyedQuantity: msg.totalTradeQuantity,
                buyOrderQuantity: msg.quantity, buyOrderStatus: msg.orderStatus,
                buyOrderExecutionTime: msg.creationTime, buyOrderPrice: msg.price,
            })
            return await this.updateOrderRegistry(orderID, "Orden de compra creada")
        } else if(msg.orderStatus === "PARTIALLY_FILLED" || msg.orderStatus === "FILLED"){
            //actualizamos estados
            let partial = msg.orderStatus === "PARTIALLY_FILLED" 
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus, buyedQuantity: msg.totalTradeQuantity, spendedQuantity: totalBuyed })
            return await this.updateOrderRegistry(orderID, "Orden de compra "+(partial ? "parcialmente" : "" )+" ejecutada" )
        //para los otros estados
        } else if (msg.orderStatus === "CANCELED") {
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de compra ha sido cancelada" )
        } else {
            await this.updateOrder(orderID, { buyOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de compra tiene un estado sin acción." )
        }
    }
    async updateSellOrderStatus(orderID, msg, totalSelled, change, changePercent){
        if(msg.orderStatus === "NEW" && msg.orderType === "LIMIT_MAKER"){
            //registramos estado de la venta nueva
            await this.updateOrder(orderID, {
                sellOrderID: msg.newClientOrderId, sellOrderQuantity: msg.quantity,
                sellOrderPrice: msg.price, sellOrderExecution: msg.creationTime, sellOrderStatus: msg.orderStatus
            })
            return await this.updateOrderRegistry(orderID, "Orden de venta creada" )
        //si una orden de compra ya se lleno o parcial
        } else if(msg.orderStatus === "PARTIALLY_FILLED" || msg.orderStatus === "FILLED"){
            let partial = msg.orderStatus === "PARTIALLY_FILLED" 
            await this.updateOrder(orderID, { 
                sellOrderStatus: msg.orderStatus, selledQuantity: msg.totalTradeQuantity, 
                receivedQuantity: totalSelled,  sellOrderType: msg.orderType, resultantQuantity: change, resultantPercentage: changePercent
            })
            return await this.updateOrderRegistry(orderID, "Orden de venta "+(partial ? "parcialmente" : "" )+" ejecutada" )
        //para los otros estados
        } else if (msg.orderStatus === "CANCELED" && msg.orderType === "LIMIT_MAKER") {
            await this.updateOrder(orderID, { sellOrderStatus: msg.orderStatus})
            return await this.updateOrderRegistry(orderID, "La orden de venta ha sido cancelada" )
        } else {
            if( msg.orderType === "LIMIT_MAKER"){
                await this.updateOrder(orderID, { sellOrderStatus: msg.orderStatus})
                return await this.updateOrderRegistry(orderID, "La orden de venta se ha ejecutado.")
            }
            
        }
    }
    async editBuyOrder(orderID, userID, enterprice, symbol){
        try {
            const {buyOrderID} = await OrdersQueue.findById(orderID)
            let response = await Exchange.getOrder(buyOrderID, userID, symbol)
            console.log("la respuesta de la orden para editar", response)
            if(response.success===0) throw response.message
            if(response.success===1 && response.order.status ==="NEW" && response.order.side ==="BUY"){
                let tobuy = response.order.origQty - response.order.executedQty
                let responseNew = await Exchange.editBuyOrder(buyOrderID, userID, symbol, tobuy, enterprice)
                if(responseNew.success === 0) throw response.message
                await OrdersQueue.findByIdAndUpdate(orderID, {
                    enterprice: enterprice
                })
                return {success: 1, newOrder: responseNew.order}
            } else {
                throw "La orden de compra no se encuentra en el estado Nueva."
            }
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error }
        }
    }
    async deleteAndSell(userID, orderID){
        const {buyOrderID, buyOrderStatus, buyedQuantity, sellOrderID, sellOrderStatus, sellOrderQuantity, symbol, tradedPair, lotSize} = await OrdersQueue.findById(orderID)
        console.log("entro en delete an sell con ", buyOrderID, buyOrderStatus, buyedQuantity, sellOrderID, sellOrderStatus, sellOrderQuantity, symbol, tradedPair, lotSize)
        if(buyOrderID && buyOrderStatus=== "NEW" && !sellOrderID){
            return await Exchange.deleteOCO(userID, symbol, buyOrderID)
        } else if(buyOrderID && sellOrderID && ( sellOrderStatus === "NEW" || sellOrderStatus ===  "PARTIALLY_FILLED" )){
            let response = await Exchange.deleteOCO(userID, symbol, sellOrderID)
            console.log("ya matamos oco", response)
            if( response.success === 0 ) return response
            let sellResp = await Exchange.panicSell(userID, tradedPair, lotSize, symbol)
            return sellResp
        }
    }
    async deleteExhgOrder(userID, origClientOrderId, symbol, skips){
        return await Exchange.deleteOCO(userID, symbol, origClientOrderId)
    }
    async sellQuantity(quantity, asset, userID, skips){
        const {lotSize} = await Exchange.getLotSize(asset)
        //console.log("todo el lotsize", lotSize)
        
        let response = await Exchange.marketSell(quantity, asset, userID, lotSize)
        console.log("respuesta de sel", response)
        return response
    }
    async regenerateSellOrder(quantity, asset, userID){
        const slot = await Exchange.getSlot(userID)
        if(slot){
            gSocket.to(slot.socketID).emit("regenerateSellOrder", {quantity, asset})
        }
    }
    async moveToHistoric(orderID, signalID){
        //console.log("LLEGA EL ID", orderID)
        let orderQueued = await OrdersQueue.findById(orderID)
        //console.log("LLEGA ", orderQueued)
        if(orderQueued){
            orderQueued = JSON.parse(JSON.stringify(orderQueued))
            delete orderQueued._id
            delete orderQueued.__v
            let hOrder = new Order(orderQueued)
            await hOrder.save()
            await OrdersQueue.findByIdAndDelete(orderID)
            //ver si cerramos senal
            let activeSignalOrders = await OrdersQueue.find({signalID})
            //si ya no tiene ordenes abiertas cerramos senal
            if(activeSignalOrders.length === 0 ){
                await Signal.findByIdAndUpdate(signalID, {active: false})
            }
            return {success: 1}
        } else {
            console.log("no existe la orden pendiente")
            return {success: 0, message: "no existe la orden pendiente"}
        }
        
    }
    async powerOffAccountApp(userID){
        
    }
}

module.exports = new Robot()