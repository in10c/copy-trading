const Order = require("../models/Order")
const OrdersQueue = require("../models/OrdersQueue")

class Orders {
    async getActiveOrders ( userID ) {
        let orders = await OrdersQueue.find({ userID })
        return orders
    }
}

module.exports = new Orders()