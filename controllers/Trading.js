var axios = require("axios").default
const Exchange = require("../services/exchange")
const Stats = require("../models/Stats")
const Transaction = require("../models/Transaction")
const WaitingPool = require("../models/WaitingPool")
const Apis = require("../models/Api")
const Order = require("../models/Order")
const OrdersQueue = require("../models/OrdersQueue")
const User = require("../models/User")
const Session = require("../models/Session")
const Slot = require("../models/Slot")
const Cluster = require("../models/Cluster")
const Constants = require("../services/constants")
const {countDecimals, fixedToPatternRounded} = require("../services/numbers")
const {sendTelegramMessage} = require("../services/telegram")


class Trading {
    //encendido y apagado del bot
    async turnOnApp(userID, status, comments){
        //obtenemos balance y sabremos si sus apis estan bien
        let responseBalance = await Exchange.getCoinBalance(userID, "BTC")
        //registro de data en stats
        let responseUpdStts = await this.updateStats(userID, status)
        if(responseUpdStts.success === 0 ) return responseUpdStts;
        //registro de transaccion
        await this.newTransact(userID, status, responseBalance.balance, comments, responseBalance.success === 0)
        
        return {success: 1, status: responseUpdStts.status}
    }
    async newTransact(userID, status, btcBalance, comments, failedToGetBalance) {
        let today = new Date()
        let trs = new Transaction({
            description: "Apagado automático de la aplicación debido a: "+comments, userID,
            affecting: 0, time: today.getTime(), balance: failedToGetBalance ? 0 : btcBalance
        })
        await trs.save()
    }
    async getStatus(userID){
        let exists = await Stats.exists({userID})
        if(!exists) return false
        let stat = await Stats.findOne({userID})
        return stat.botStatus
    }
    async updateStats(userID, botStatus){
        try {
            let exists = await Stats.exists({userID})
            let response;
            if(exists){
                response = await Stats.findOneAndUpdate({userID}, { botStatus})
                console.log("la respuesta de actualizar", response)
            } else {
                let newStat = new Stats({ userID, botStatus })
                response = await newStat.save()
                console.log("la respuesta de actualizar", response)
            }    
            return {success: 1, status: response.botStatus}  
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}   
        }
    }
    async prenderBotsdeTodos(){
        await Stats.deleteMany({})
        let allApis = await Apis.find({})
        let counter = 0
        do {
            let userApis = allApis.pop()
            let savedCorrectly = await this.turnOnApp(userApis.userID)
            if(!savedCorrectly) {
                console.log("Error al prender el bot")
            } else {
                counter += 1
                console.log("Se prendio correctamente el bot")
            }
        //} while ( allApis.length > 0 && counter <= 20 );
    } while ( allApis.length > 0 );
    }
    async getReadyBots(){
        return await Stats.find({botStatus: true})
    }
    async updateAnalytics(clientsParticipating){
        //we clean the workspace
        let today = new Date()
        const {askPrice : bnbPrice, success, message} = await Exchange.getAskPrice("BNB")
        if(success === 0) return {success, message}
        const {lotSize, success: succLot, message: messLot, MIN_NOTIONAL} = await Exchange.getLotSize("BNB")
        if(succLot === 0) return {success: succLot, message: messLot}
        console.log("trajo de lotsize", lotSize, MIN_NOTIONAL)
        const stepSize = parseFloat(lotSize.stepSize)
        const decimalsStepSize = countDecimals(stepSize)
        let completeMinMarket = (parseFloat(MIN_NOTIONAL.minNotional) / bnbPrice)
        let minMarketBNB = fixedToPatternRounded(completeMinMarket, stepSize)
        console.log("el minmarket", minMarketBNB)
        let auxminqty = parseFloat(lotSize.minQty)
        const minBNBQty = auxminqty >= minMarketBNB ? auxminqty : minMarketBNB
        let allClients = await Session.find({})
        allClients.sort(this.sortOrders)
        let firstItems = allClients.slice(0, clientsParticipating)
        //console.log("las anals", firstItems)
        let ready = []
        let notReady = []
        let totalInBTC = 0
        if(firstItems.length === 0 ) return {success: 1, ready, notReady}
        do {
            //hacemos el proceso sincrono por cada uno de los clientes
            const sess = firstItems.pop()
            const client = await User.findById(sess.userID)
            let { success, message, allBalances } = await Exchange.getAllBalances(sess.userID)
            if(success === 1){
                //console.log("todos los balances son", allBalances)
                let requiredBalances = {}
                for (let index = 0; index < allBalances.length; index++) {
                    //console.log("paso por en allbalances", index, allBalances.length)
                    const element = allBalances[index];
                    if( ["BTC", "BNB"].includes(element.asset) ){
                        requiredBalances[element.asset] = element.free
                    }
                }
                let pendingOrders = await OrdersQueue.find({userID: sess.userID})

                if(!requiredBalances["BTC"] || isNaN(requiredBalances["BTC"])){
                    let aux = {
                        time: today.getTime(), type: "Error", comments: "No pudo traer balance de Bitcoin."
                    }
                    notReady.push({...JSON.parse(JSON.stringify(sess)), ...aux})
                    await Session.findByIdAndUpdate(sess._id, aux) 
                } else {
                    let {success, message, balancePortion, totalBalanceWithOrders, yetinorder, limits, percentageToUse} = await Exchange.getAnalyticsData(sess.userID, client.selectedPackage, requiredBalances["BTC"], pendingOrders)
                    if(success === 0) {
                        let aux = {
                            time: today.getTime(), type: "Error", comments: message
                        }
                        notReady.push({...JSON.parse(JSON.stringify(sess)), ...aux})
                        await Session.findByIdAndUpdate(sess._id, aux)
                        //return { success: 0, message }
                    } else {
                        let minimumBNB = ( balancePortion * 0.0015 ) / bnbPrice
                        console.log("paso x minimo bnb", minimumBNB, requiredBalances["BNB"])
                        //no tiene el minimo para hacer operaciones
                        if(requiredBalances["BNB"] <= minimumBNB){
                            console.log("tenemos que comprar BNB", decimalsStepSize)
                            let completeRequiredBNB = (( ( balancePortion * 0.0075 ) / bnbPrice ) - requiredBalances["BNB"])
                            //console.log("complete required", completeRequiredBNB)
                            let requiredBNB = fixedToPatternRounded(completeRequiredBNB, stepSize)
                            
                            let toBuy = requiredBNB > minBNBQty ? requiredBNB : minBNBQty
                            console.log("a comprar de BNB", requiredBNB, toBuy)
                            let responseMarketBuy = null
                            if(Constants.production){
                                responseMarketBuy = await Exchange.marketBuy(sess.userID, "BNB", toBuy)
                            } else {
                                responseMarketBuy = {success: 0, message: "Se apago la compra de BNB manualmente"}
                            }
                            let aux = null
                            if(responseMarketBuy.success === 0 ){
                                aux = {
                                    time: today.getTime(), type: "Error", comments: "No se ha podido comprar BNB al cliente porque: "+responseMarketBuy.message+"."
                                }
                            } else {
                                let time = today.getTime()
                                aux = {
                                    time, type: "Error", comments: "Se le ha comprado la cantidad de "+toBuy+" BNB al cliente, estará disponible para el próximo trade."
                                }
                                let maarketBuyTX = new Transaction({
                                    description: "Compra de BNB a mercado para hacer pago de comisiones.",
                                    balance: toBuy,
                                    time, userID: sess.userID
                                })
                                await maarketBuyTX.save()
                            }
                            
                            notReady.push({...JSON.parse(JSON.stringify(sess)), ...aux})
                            await Session.findByIdAndUpdate(sess._id, aux)
                            
                        //si tiene bnb para hacer operaciones
                        } else {
                            totalInBTC += balancePortion
                            let newAux = {
                                btcBalance: requiredBalances["BTC"],  totalBalanceWithOrders,
                                btcPortion: balancePortion, btcInOrders: yetinorder, percentageToUse: ( percentageToUse * 100 ),
                                bnbBalance: requiredBalances["BNB"], limits, time: today.getTime(), type: "Ready"
                            }
                            ready.push({...JSON.parse(JSON.stringify(sess)), ...newAux})
                            await Session.findByIdAndUpdate(sess._id, newAux)
                            console.log("paso lineal x ", sess.userID, requiredBalances, "la data", success, message, balancePortion, totalBalanceWithOrders, yetinorder, limits)
                        }
                        
                    }
                }
            } else {
                let aux = {
                    time: today.getTime(), type: "Error", comments: message
                }
                notReady.push({...JSON.parse(JSON.stringify(sess)), ...aux})
                await Session.findByIdAndUpdate(sess._id, aux)
                console.log("no encontre los balances", message)
            }
            
            
        } while (firstItems.length > 0);

        return {success: 1, ready, notReady, totalInBTC, bnbPrice}
    }
    async getQueued(){
        let pendingOrders = await Session.find({type: {$ne: "Borned"}})
        let ready = []
        let notReady = []
        for (let index = 0; index < pendingOrders.length; index++) {
            const element = pendingOrders[index];
            if(element.type === "Ready"){
                ready.push(element)
            } else if(element.type === "Error"){
                notReady.push(element)
            }
        }
        return {ready, notReady}
    }
    async getMainQueue(queueAmount, activeClusters, inPair){
        let ourClusters = JSON.parse(JSON.stringify(activeClusters))
        let amountToSend = parseFloat(queueAmount)
        let {ready} = await this.getQueued()
        //console.log("antes de ordenarse estaba asi", ready)
        ready.sort(this.sortOrders)
        //console.log("despues de ordenarse quedo", ready)
        let sumQueued = 0
        let queue = []
        do {
            const item = ready.shift()
            if(!(( sumQueued + item.btcPortion ) > amountToSend)){
                let newItem = JSON.parse(JSON.stringify(item))
                let beforeTraded = await OrdersQueue.find({userID: item.userID, tradedPair: inPair})
                //ya no le compramos si ya tiene el mismo asset
                if(beforeTraded.length > 0){
                    newItem.skiped = true
                    newItem.comments = "El usuario ya tiene tradeando este par."
                    let auxLink = Constants.appUrl()+"/account/"+item.userID

                    await sendTelegramMessage("El cliente "+item.email+" no tradeara el par "+inPair+" porque estaría repetido. <a href='"+auxLink+"'>Revisar cliente.</a>")
                    queue.push(newItem)
                } else {
                    //revisamos ahora si no tiene un balance protegido mayor a su balance, sino puede tronar
                    let {success, balance, message } = await Exchange.getCoinBalance(item.userID, inPair)
                    //si hubo errores el traer el balance de la altcoin
                    if(success === 0 ) {
                        newItem.skiped = true
                        newItem.comments = "No se ha podido traer el balance de la altcoin con el cliente porque: "+message+"."
                        let auxLink = Constants.appUrl()+"/account/"+item.userID
                        await sendTelegramMessage("No se ha podido traer el balance de la altcoin del cliente "+item.email+" porque: "+message+". <a href='"+auxLink+"'>Revisar cliente.</a>")
                        queue.push(newItem)
                    //si no hubo errores continuamos
                    } else {
                        //primero revisamos si compramos la altcoin no este protegida de mas 
                        let protectedAltcoin = await Exchange.getProtected(item.userID, inPair)
                        if(balance < protectedAltcoin) {
                            newItem.skiped = true
                            newItem.comments = "El balance de la altcoin a comprar es menor a su cantidad protegida."
                            let auxLink = Constants.appUrl()+"/account/"+item.userID
                            await sendTelegramMessage("El balance de la altcoin a comprar es menor a su cantidad protegida, cliente: "+item.email+". <a href='"+auxLink+"'>Revisar cliente.</a>")
                            queue.push(newItem)
                            //"El balance de la altcoin a comprar es menor a su cantidad protegida."
                        } else {
                            let clientSlot = await Slot.findOne({userID: item.userID})
                            //si ya tiene VPS activo
                            if(clientSlot){
                                //console.log("vamos a verificar el socket", gSocket.sockets.connected)
                                if(gSocket.sockets.connected[clientSlot.socketID] && gSocket.sockets.connected[clientSlot.socketID].connected){
                                    //console.log("si esta conectado")
                                    newItem.assignedSocket = clientSlot.socketID
                                    newItem.socketOrCluster = "socket"
                                    newItem.skiped = false
                                } else {
                                    console.log("no esta conectado")
                                    var room = gSocket.sockets.adapter.rooms[item.userID];
                                    //console.log("el rooom es ", room)
                                    if(room){
                                        //if(room.length > 1) console.log("el room tiene mas de un vps", room, item) 
                                        let newSocketID = Object.keys(room.sockets)[0]
                                        console.log("el nuevo socket id es ", newSocketID)
                                        await Slot.findOneAndUpdate({userID: item.userID}, {socketID: newSocketID})
                                        newItem.assignedSocket = newSocketID
                                        newItem.socketOrCluster = "socket"
                                        newItem.skiped = false
                                        newItem.comments = "Se ha recuperado el socket del cliente."
                                    } else {
                                        newItem.skiped = true
                                        newItem.comments = "El socket del cliente ha muerto."
                                        let auxLink = Constants.appUrl()+"/account/"+item.userID
                                        console.log("al link", auxLink)
                                        await sendTelegramMessage("El socket del cliente ha muerto. <a href='"+auxLink+"'>Revisar cliente.</a>")
                                    }
                                }
                            } else {
                                let firstkey = Object.keys(ourClusters)[0]
                                newItem.assignedCluster = firstkey
                                newItem.socketOrCluster = "cluster"
                                newItem.skiped = false
                                ourClusters[firstkey].free -= 1
                                if(ourClusters[firstkey].free === 0 ){
                                    delete ourClusters[firstkey]
                                }
                            }
                            sumQueued += newItem.btcPortion
                            queue.push(newItem)
                        }
                    }                
                }
            }
        } while (sumQueued < amountToSend && ready.length > 0);
        return queue
    }
    async buyAsset(asset, quantity, buyType){
        console.log("llego a buy asset con", asset, quantity, buyType)
        const {success, lotSize, message, MIN_NOTIONAL} = await Exchange.getLotSize(asset )
        if(success === 0) return {success, message}
        console.log("la respuesta de lot size", lotSize, quantity)
        if( quantity > parseFloat(lotSize.maxQty)) return {success : 0 , message: "La cantidad a comprar es mayor a la permitida por el exchange."}
        if( quantity < parseFloat(lotSize.minQty)) return {success : 0 , message: "La cantidad a comprar es menor a la permitida por el exchange."}
        const response = await Exchange.getAskPrice(asset)
        if(response.success === 0) return {success:0, message: response.message}
        const value = quantity * response.askPrice
        if(value < parseFloat(MIN_NOTIONAL.minNotional)) return {success:0, message: "El valor de la compra es menor al permitido por el exchange."}
        let {ready} = await this.getQueued()
        for (let index = 0; index < ready.length; index++) {
            const {userID} = ready[index];
            let response = await Exchange.buyAsset(userID, asset, quantity, buyType, lotSize.stepSize)
        }
        return {success: 1}
    }
    async getCriptoAccount(userID){
        const response = await Exchange.getAllBalances(userID)
        if(response.success === 0) return { success: 0, message: response.message}
        const { allBalances } = response
        let nonZeroBalances = allBalances.filter(val=>(val.free > 0 || val.locked > 0))
        const protectdBalancs = await Exchange.getAllProtected(userID)
        console.log("la resp de protectd", protectdBalancs)
        if(protectdBalancs){
            //acomodamos los balances para mno hacer tantos loops
            let { balances } = protectdBalancs
            console.log("los balances son proye", balances)
            let auxProtected = {}
            for (let idx = 0; idx < balances.length; idx++) {
                const itm = balances[idx];
                auxProtected[itm.asset] = itm.quantity
            }
            //ya con los balances protegidos acomodados ponemos en los balances
            for (let index = 0; index < nonZeroBalances.length; index++) {
                if(auxProtected[nonZeroBalances[index].asset]){
                    nonZeroBalances[index].protected = auxProtected[nonZeroBalances[index].asset]
                }
            }
        }
        const respOrders = await Exchange.getExchangeOrders(userID)
        if(respOrders.success === 0) return { success: 0, message: respOrders.message}
        const slot = await Slot.findOne({userID})
        let isSocketConnected = false
        if(slot && gSocket.sockets.connected[slot.socketID] && gSocket.sockets.connected[slot.socketID].connected){
            isSocketConnected = true   
        }
        const user = await User.findById(userID)
        const appOpenOrders = await OrdersQueue.find({userID})
        return { success: 1, nonZeroBalances, openOrders: respOrders.openOrders, slot, user, isSocketConnected, appOpenOrders}
    }
    async updateOrdersData(){
        //we clean the workspace
        let today = new Date()
        let correctMonth = (today.getMonth()+1)+""
        correctMonth = correctMonth.length == 2 ? correctMonth : "0"+correctMonth
        let beginDate = today.getDate()+""
        beginDate = beginDate.length == 2 ? beginDate : "0"+beginDate
        let endDate = (today.getDate()+1)+""
        endDate = endDate.length == 2 ? endDate : "0"+endDate
        let minStrDate = today.getFullYear()+"-"+correctMonth+"-"+beginDate+"T00:00:00.000Z"
        let maxStrDate = today.getFullYear()+"-"+correctMonth+"-"+endDate+"T00:00:00.000Z"
        await Session.deleteMany({})
        let analitycs = await this.getReadyBots()
        //console.log("las anals", analitycs)
        let ready = []
        if(analitycs.length === 0 ) return {success: 1, sessions: ready}
        do {
            //hacemos el proceso sincrono por cada uno de los clientes
            let { userID } = analitycs.pop()
            const client = await User.findById(userID)
            //console.log("las fechas terminan siendo", minStrDate, maxStrDate)
            let pendingOrders = await OrdersQueue.find({userID})
            // let historicOrders = await Order.find({userID, transactTime: {
            //     $gte: minStrDate,
            //     $lte: maxStrDate
            // },
            //     buyOrderStatus: { $ne : "CANCELED"},
            // })
            let historicOrders = await Order.find({userID, buyOrderStatus: { $ne : "CANCELED"} })
            
            //console.log("las ordenes son", pendingOrders.length, historicOrders.length, pendingOrders, historicOrders)
            let aux = {
                userID, email: client.email,  openOrders: pendingOrders.length,
                historicOrders: historicOrders.length, time: today.getTime(), type: "Borned"
            }
            ready.push(aux)
            await new Session(aux).save()

        } while (analitycs.length > 0);

        ready.sort(this.sortOrders)

        return {success: 1, sessions : ready}
    }
    sortOrders (a,b){
        //return ( a.openOrders + a.historicOrders) - ( b.openOrders + b.historicOrders )
        if (a.openOrders > b.openOrders) {
            return 1;
        } else if (a.openOrders < b.openOrders) { 
            return -1;
        }
        if (a.historicOrders < b.historicOrders) { 
            return -1;
        } else if (a.historicOrders > b.historicOrders) {
            return 1
        } else { // nothing to split them
            return 0;
        }
    }
    async verificateSymbolBalance(tradList){
        let logs = {}
        for (let index = 0; index < tradList.length; index++) {
            const {userID, tradedPair, index : indice} = tradList[index];
            const {locked, balance} = await Exchange.getCoinBalance(userID, tradedPair)
            const {numberOfBuyOrders, numberOfSellOrders} = await Exchange.symbolOpenOrders(userID, tradedPair+"BTC")
            logs[indice] = "Balance: "+balance+"\nLocked: "+locked+"\nCompra: "+numberOfBuyOrders+"\nVenta: "+numberOfSellOrders
            await new Promise(r => setTimeout(r, 100));
        }
        return {success: 1, logs}
    }
    async clusterOcuppation(){
        const clusters = await Cluster.find({})
        const slots = await Slot.find({})
        let clusterOcupation = {}
        //generamos la estructura de clusters
        for (let index = 0; index < clusters.length; index++) {
            let clusterNumber = clusters[index].clusterNumber
            clusterOcupation[clusterNumber] = {
                used: 0
            }
        }
        for (let index = 0; index < slots.length; index++) {
            let element = slots[index];
            clusterOcupation[element.clusterNumber] = {
                used: ( clusterOcupation[element.clusterNumber].used + 1 )
            }
        }
        return {success: 1, clusterOcupation}
    }
    async getActiveClusters(){
        let out = {}
        let max = 30
        let {clusterOcupation} = await this.clusterOcuppation()
        Object.keys(clusterOcupation).map(index=>{
            if(clusterOcupation[index].used < max) {
                out[index] = {
                    used: clusterOcupation[index].used,
                    free: max - clusterOcupation[index].used
                }
            }
        }) 
        return  out   
    }
    async refreshSocketOrders(userID){
        const { socketID } = await Slot.findOne({userID})
        console.log("vamos a emitir un evento al socket", socketID)
        gSocket.to(socketID).emit("refreshOrders")
        return {success: 1}
    }
    
    async reBornVPS(userID, clusterNumber){
        const clu = await Cluster.findOne({clusterNumber})
        const address = Constants.production ? clu.address : clu.testAddress
        let response = await axios.post(address+"/generateVPS", {usersList: [{userID}]})
        console.log("respuesta de generate vps", response.data)
        return {success: 1}
    }
    async deleteVPS(userID){
        const { socketID } = await Slot.findOne({userID})
        console.log("vamos a emitir un evento al socket", socketID)
        gSocket.to(socketID).emit("deleteVPS")
        return {success: 1}
    } 
    async deleteSlot(userID){
        await Slot.findOneAndDelete({userID})
        return {success: 1}
    }
    async getVPSWOOrders(){
        let log = ""
        let slots = await Slot.find({})
        for (let index = 0; index < slots.length; index++) {
            let {userID} = slots[index];
            let pendingOrders = await OrdersQueue.find({userID})
            if(pendingOrders.length === 0){
                log+= "El usuario "+userID+" no tiene ordenes abiertas y si tiene un VPS.\n"
            }
        }
        return {success: 1, log}
    }
    async getBTCBalance(userID){
        let btcBalance = await Exchange.getCoinBalance(userID, "BTC")
        return btcBalance
    }
    async recoverySocketID(userID){
        console.log("no esta conectado")
        var room = gSocket.sockets.adapter.rooms[userID];
        if(room){
            if(room.length === 0) return {success: 0, message: "El usuario no tiene proceso en ejecución"}
            console.log("el rooom es ", room)
            let newSocketID = Object.keys(room.sockets)[0]
            console.log("el nuevo socket id es ", newSocketID)
            await Slot.findOneAndUpdate({userID}, {socketID: newSocketID})
            return {success: 1}
        } else {
            return {success: 0, message: "El usuario no tiene ningun socket activo"}
        }
        
    }
    async sellDust(userID) {
        return await Exchange.sellDust(userID, "BTC")
    }
    async regenerateQuantities(account, inPair, initialOrderID, orderID, orderType){
        let response = await Exchange.regenerateQuantities(account, inPair, initialOrderID)
        if(response.success === 0 ) return response
        let aux = {
            spendedQuantity: response.totalBuyed,
            receivedQuantity: response.totalSelled,
            stats: {
                buyOrders: response.buyOrders,
                sellOrders: response.sellOrders,
                neutralOrders: response.neutralOrders,
                totalBuyed: response.totalBuyed,
                totalSelled: response.totalSelled,
            }
        }
        if(
            response.totalBuyed && response.totalBuyed > 0 &&
            response.totalSelled && response.totalSelled > 0 &&
            response.change && !isNaN(response.change) &&
            response.percentChage && !isNaN(response.percentChage)){
                aux.resultantQuantity= response.change
                aux.resultantPercentage= response.percentChage
        }
        console.log("pasamos con aux", aux)
        if(orderType === "Orden abierta"){
            console.log("con orden abierta")
            await OrdersQueue.findByIdAndUpdate(orderID, aux )
            return {success: 1}
        } else {
            await Order.findByIdAndUpdate(orderID, aux )
            return {success: 1}
        }
    }
}

module.exports = new Trading()