var mongoose =  require('mongoose');
var child_process = require("child_process");
var axios = require("axios").default
const Binance = require('binance-api-node').default
const Queue = require("../models/Queue")
const Signal = require("../models/Signal")
const Order = require("../models/Order")
const OrdersQueue = require("../models/OrdersQueue")
const Cluster = require("../models/Cluster");
const Slot = require('../models/Slot');
const Constants = require('../services/constants');
const exchange = require('../services/exchange');
const {sendTelegramMessage} = require("../services/telegram")

const binanceClient = Binance()
//memory client bot handler
let childProceses = {}

class Queues {
    //bot creation
    disperseOrder(inPair, outPair, sl, buyPrice, gain, clientsQueue) {
        let type = "BUY";
        return new Promise(async (resolve, reject)=>{
            try {
                let infoExch = await binanceClient.exchangeInfo()
                let symbol = inPair+""+outPair
                let ticker = await binanceClient.dailyStats({ symbol })
                let symbolData = infoExch.symbols.filter(val=> val.symbol === symbol)[0]
                //verificamos filtros de precio !importante
                let impediments = this.verifyFilters(symbolData.filters, buyPrice, ticker.askPrice, sl, gain)
                if (impediments) {
                    throw impediments
                } else {
                    let lotsize = symbolData.filters.filter(val=> val.filterType==="LOT_SIZE")[0]
                    //let queue = await Queue.find({})
                    let today = new Date()
                    let transactTime = today.getTime()
                    //console.log("la cola es ", queue)
                    let sign = new Signal({
                        kind: "spot", inPair, outPair,
                        entryPrice: buyPrice,
                        stopLoss: sl, registeredTime: transactTime,
                        takeProfit: gain,type, active: true
                    })
                    let signal = await sign.save()

                    clientsQueue.map(async (val)=>{
                        await this.createChild(val, inPair, outPair, sl, buyPrice, gain, type, signal._id, parseFloat(lotsize.stepSize), transactTime)
                    })
                    await this.createSocketsOnCluster(clientsQueue, symbol, parseFloat(lotsize.stepSize), buyPrice)
                    console.log("termino con todos los maps")
                    resolve(signal._id)
                }
            } catch (error) {
                console.log("error en dispersar orden: ", error)
                reject(typeof error === "string" ? error : error.message)
            }
        })
    }
    async createSocketsOnCluster(queue, symbol, stepSize, buyPrice){
        //console.log("llego la cola al queue", queue)
        let tosend = {}
        let clusterStake = []
        for (let index = 0; index < queue.length; index++) {
            const element = queue[index];
            if(element.socketOrCluster != "socket"){ 
                if(!tosend[element.assignedCluster]){
                    let clu = await Cluster.findOne({clusterNumber: element.assignedCluster})
                    //console.log("elige cual addres ", Constants.production , clu.address , clu)
                    tosend[element.assignedCluster] = {
                        toBorn: [],
                        address: Constants.production ? clu.address : clu.testAddress
                    }
                    clusterStake.push(element.assignedCluster)
                }
                tosend[element.assignedCluster].toBorn.push(element)
            }
        }
        //console.log("al final el envio queda", tosend)
        for (let idxx = 0; idxx < clusterStake.length; idxx++) {
            const clusterNumber = clusterStake[idxx];
            const aux = tosend[clusterNumber]
            //console.log("vamos a enviar al cluster", clusterNumber, aux.address, aux.toBorn)
            let response = await axios.post(aux.address+"/generateVPS", {usersList: aux.toBorn})
            //console.log("respuesta de generate vps", response.data)
        }
    }
    async verifyNewEnterPrice(signalID, symbol, newEnterPrice){
        try {
            const signal = await Signal.findById(signalID)

            let infoExch = await binanceClient.exchangeInfo()
            let ticker = await binanceClient.dailyStats({ symbol })
            let symbolData = infoExch.symbols.filter(val=> val.symbol === symbol)[0]
            //verificamos filtros de precio !importante
            let impediments = this.verifyFilters(symbolData.filters, newEnterPrice, ticker.askPrice, signal.stopLoss, signal.takeProfit, true)
            if (impediments) {
                return {success: 0, message: impediments}
            } else {
                return {success: 1}
            }
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async verifyNewSellPoints(signalID, symbol, newGain, newSL){
        try {
            const signal = await Signal.findById(signalID)
            let infoExch = await binanceClient.exchangeInfo()
            let ticker = await binanceClient.dailyStats({ symbol })
            let symbolData = infoExch.symbols.filter(val=> val.symbol === symbol)[0]
            //verificamos filtros de precio !importante
            let impediments = this.verifyFilters(symbolData.filters, signal.entryPrice, ticker.askPrice, newSL, newGain, true)
            if (impediments) {
                return {success: 0, message: impediments}
            } else {
                return {success: 1}
            }
        } catch (error) {
            return {success: 0, message: error.message ? error.message : error}
        }
    }
    async createChild(queueItem, inPair, outPair, sl, buyPrice, gain, type, signalID, lotSize, transactTime){
        let processID = mongoose.mongo.ObjectID();
        let symbol = inPair+""+outPair
        let order = { userID: queueItem.userID, symbol, tradedPair: inPair, processID, sl, enterprice: buyPrice, gain, side: type, 
            signalID, globalStatus: "Proceso iniciado", transactTime, lotSize, pretendedToBuy: queueItem.btcPortion
        }
        console.log("guardi la orden del child")
        
        if(queueItem.socketOrCluster === "socket"){
            let response = await exchange.createServerBuyOrder(queueItem.userID, queueItem.btcPortion, symbol, buyPrice, lotSize)
            if(response.success===0){
                let auxLink = Constants.appUrl()+"/account/"+queueItem.userID
                await sendTelegramMessage("Ha ocurrido un error al generar la orden de compra en el servidor copy-trading con el usuario "+queueItem.userID+" con el par "+symbol+" porque: "+response.message+". <a href='"+auxLink+"'>Revisar cliente.</a>")
            } else {
                const {newOrder, buyBalance} = response
                order = {...order, buyOrderID: newOrder.clientOrderId, buyedQuantity: buyBalance, 
                    buyOrderQuantity: newOrder.origQty, buyOrderStatus: newOrder.status, initialOrderID : newOrder.orderId,
                    buyOrderExecutionTime: newOrder.transactTime, buyOrderPrice: newOrder.price, logs: [
                        {
                            description: "Éxito al crear la orden de compra",
                            time: transactTime
                        }
                    ], globalStatus : "Éxito al crear la orden de compra"
                }
                await new OrdersQueue(order).save()
            }
        //si le toca cluster solo guardamos
        } else {
            await new OrdersQueue(order).save()
        } 
        
    }
    verifyFilters(filters, buyPrice, currentPrice, sl, gain, skip){
        if(gain <= buyPrice && !skip){
            return "El precio de toma de beneficios debe ser mayor al precio de compra"
        }
        if(sl >= buyPrice && !skip){
            return "El precio de pare de perder debe ser menor al precio de compra"
        }
        console.log("en verify el current es ", currentPrice)
        let priceFilter = filters.filter(val=> val.filterType==="PRICE_FILTER")[0]
        let percentPrice = filters.filter(val=> val.filterType==="PERCENT_PRICE")[0]
        if( buyPrice < parseFloat(priceFilter.minPrice) ){
            return "El precio es menor al precio mínimo establecido de compra"
        } 
        if( buyPrice > parseFloat(priceFilter.maxPrice) ){
            return "El precio es mayor al precio máximo establecido de compra"
        }
        let minPercentage = currentPrice * parseFloat(percentPrice.multiplierDown)
        let maxPercentage = currentPrice * parseFloat(percentPrice.multiplierUp)
        if( buyPrice < minPercentage ){
            return "El precio es menor al precio mínimo protegido"+minPercentage
        } 
        if( buyPrice > maxPercentage ){
            return "El precio es mayor al precio máximo protegido: "+maxPercentage
        }
        return false
    }
    //bot interaction
    deleteOrder(processID){
        return new Promise((resolve, reject)=>{
            childProceses[processID].send({type: "delete"})
            resolve()
        })
    }
    oldDeleteOrders(signalID, symbol){
        return new Promise((resolve, reject)=>{
            Object.keys(childProceses).map(userID=>{
                childProceses[userID]["process"].send({type: "delete", symbol})
            })
            resolve()
        })
    }
    async deleteOrders(signalID, symbol, tradList, asset){
        const response = await exchange.getLotSize(asset )
        if(response.success=== 0) return response
        const minNotional = parseFloat(response.MIN_NOTIONAL.minNotional)
        console.log("el min notional", minNotional)
        for (let index = 0; index < tradList.length; index++) {
            //por cada orden sacamos el userID para obtener el socket
            const { userID, _id: bwOrderID, lotSize } = tradList[index];
            await this.killToClient(userID, symbol, minNotional, asset, lotSize)
        }
        return {success: 1}
    }
    async editOCO(signalID, sl, gain, symbol, tradList) {
        for (let index = 0; index < tradList.length; index++) {
            //por cada orden sacamos el userID para obtener el socket
            const { userID, lotSize, _id : bwOrderID, tradedPair } = tradList[index];
            console.log("el usuario que se saco de cada uno", userID)
            await this.editClientSellOrders(userID, symbol, lotSize, sl, gain, bwOrderID, tradedPair)
        }
        await Signal.findByIdAndUpdate(signalID, {
            stopLoss: sl,
            takeProfit: gain
        })
    }
    async editBuyOrders(signalID, enterprice, symbol, tradList){
        for (let index = 0; index < tradList.length; index++) {
            //por cada orden sacamos el userID para obtener el socket
            const { userID, lotSize, _id: bwOrderID } = tradList[index];
            await this.editClientBuyOrder(userID, symbol, lotSize, enterprice, bwOrderID)
        }
        await Signal.findByIdAndUpdate(signalID, {
            entryPrice: enterprice
        })
    }
    async editClientBuyOrder(userID, symbol, lotSize, enterprice, bwOrderID){
        let {success, message, filtrdOrdrs, numberOfOpenOrders} = await exchange.symbolFiltrOpenOrders(userID, symbol, "BUY")
        if(success===0) {
            await this.sendDebugMessage("No se han podido traer las ordenes abiertas del usuario para editarlas, usuario "+userID+" con el par "+symbol+" debido a: "+message, userID)
        } else {
            if(numberOfOpenOrders === 0) await this.sendDebugMessage("No tiene ordenes abiertas de compra para editar, usuario "+userID+" con el par "+symbol, userID)
            if(numberOfOpenOrders > 1) await this.sendDebugMessage("Las ordenes abiertas son mayores a 1, usuario "+userID+" con el par "+symbol, userID)
            if(numberOfOpenOrders === 1){
                const {origQty, executedQty, clientOrderId} = filtrdOrdrs[0];
                let toBuyAmout = origQty - executedQty
                const {success: succBO, message : msgBO, order: newOrder} = await exchange.editBuyOrder(clientOrderId, userID, symbol, toBuyAmout, enterprice, lotSize)
                if(succBO === 1){
                    await OrdersQueue.findByIdAndUpdate(bwOrderID, {
                        enterprice: enterprice
                    })
                    await this.changeOrderStatus(bwOrderID, "Se ha editado la orden de compra correctamente, el nuevo ID de la orden es: "+newOrder.clientOrderId+" con precio: "+newOrder.price+" comprando: "+newOrder.origQty)
                } else {
                    await this.sendDebugMessage("Ha habido un error al editar la orden de compra, usuario "+userID+" con el par "+symbol+" debido a: "+msgBO, userID)
                }
            }
        }
    }
    async killToClient(userID, symbol, MIN_NOTIONAL, asset, lotSize) {
        let {success, message, numberOfOpenOrders, filtrdOrdrs} = await exchange.symbolFiltrOpenOrders(userID, symbol)
        if(success === 0 ) {
            await this.sendDebugMessage("No se han podido traer las ordenes abiertas del usuario para hacer venta de pánico, usuario "+userID+" con el par "+symbol+" debido a: "+message, userID)
        } else {
            if(numberOfOpenOrders > 0) {
                let {success: succCO, message: msgCO} = await exchange.cancelOrders(userID, symbol, filtrdOrdrs)
                if(succCO === 0){
                    await this.sendDebugMessage("Error al eliminar las ordenes abiertas, usuario "+userID+" con el par "+symbol+" debido a: "+msgCO, userID)
                } else {
                    //timeout necesario para que de nuevo el balance sea el correcto despues de cancelar ordenes
                    await new Promise(r => setTimeout(r, 2000));
                    let response = await exchange.panicSell(userID, asset, lotSize, symbol, MIN_NOTIONAL)
                    if(response.success=== 0){
                        await this.sendDebugMessage("Error al hacer venta de pánico, usuario "+userID+" con el par "+symbol+" debido a: "+response.message, userID)
                    }
                }
            } else {
                await this.sendDebugMessage("El número de ordenes abiertas al hacer venta de pánico es 0, usuario "+userID+" con el par "+symbol+" debido a: "+message, userID)
            }
        }
    }
    async editClientSellOrders(userID, symbol, lotSize, sl, gain, bwOrderID, tradedPair){
        let {success, message, filtrdOrdrs, numberOfOpenOrders} = await exchange.symbolFiltrOpenOrders(userID, symbol, "SELL")
        if(success===0) {
            await this.sendDebugMessage("No se han podido traer las ordenes abiertas del usuario para editar su venta, usuario "+userID+" con el par "+symbol+" debido a: "+message, userID)
        } else {
            if(numberOfOpenOrders === 0) await this.sendDebugMessage("No tiene ordenes abiertas de venta para editar, usuario "+userID+" con el par "+symbol, userID)
            if(numberOfOpenOrders > 2) await this.sendDebugMessage("Las ordenes abiertas son mayores a 2, usuario "+userID+" con el par "+symbol, userID)
            if(numberOfOpenOrders === 2){
                console.log("todo ha salido bien, las ordenes", filtrdOrdrs)
                let {success: succCO, message: msgCO} = await exchange.cancelOrders(userID, symbol, filtrdOrdrs)
                if(succCO === 0){
                    await this.sendDebugMessage("No se pudieron eliminar las ordenes de venta, usuario "+userID+" con el par "+symbol+" porque: "+msgCO, userID)
                } else{
                    let {success: succNO, message: messNO, orderID: newOrderID, price, qty } = await exchange.newOCO(userID, tradedPair, lotSize, symbol, sl, gain, false)
                    if( succNO === 1 ){
                        await this.changeOrderStatus(bwOrderID, "Se ha editado la orden de venta correctamente, el nuevo ID de la orden es: "+newOrderID+" con precio: "+price+" vendiendo: "+qty)
                    } else {
                        await this.sendDebugMessage("Error al crear la orden de venta editada, usuario "+userID+" con el par "+symbol+" debido a que: "+messNO, userID)
                    }
                }
            }
        }
    }
    async changeOrderStatus(orderID, newStatus){
        let today = new Date()
        let currentOrder = await OrdersQueue.findById(orderID)
        currentOrder.globalStatus = newStatus
        if(!currentOrder.logs){
            currentOrder.logs = []
        }
        currentOrder.logs.push({
            description: newStatus,
            time: today.getTime()
        })
        await currentOrder.save()
    }
    returnRobots(){
        let returningArray = []
        Object.keys(childProceses).map(userID=>{
            returningArray.push({
                userID,
                status: childProceses[userID].status,
                logs: childProceses[userID].logs
            })
        })  
        return returningArray
    }
    async sendDebugMessage(msg, userID){
        let auxLink = Constants.appUrl()+"/account/"+userID
        await sendTelegramMessage(msg+" <a href='"+auxLink+"'>Revisar cliente.</a>")
    }
    async signalsHistory(){
        return await Signal.find({active: {$ne: true}})
    } 
    
    async activeSignals(){
        return await Signal.find({active: true})
    }
}

module.exports = new Queues()