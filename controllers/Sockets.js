const Slot = require("../models/Slot")

class Sockets {
    async createSlot(userID, socketID, clusterNumber, reBorn){
        console.log("create slot", userID, socketID, clusterNumber, reBorn)
        let sloott = await Slot.findOne({userID})
        if(reBorn || sloott){
            await Slot.findOneAndUpdate({userID}, {socketID, reBorned: true})
        } else { 
            let slt = new Slot({
                userID, clusterNumber, socketID
            })
            await slt.save()
        }
    }
}

module.exports = new Sockets()