var express = require('express');
var bodyParser = require('body-parser')
var cors = require('cors')

const Trading = require("./controllers/Trading")
const OrderKeepers = require("./controllers/OrderKeepers")
const { verifyAuthToken } = require('./services/jsonWebToken')
const verifyDeveloperKey = require('./services/verifyDeveloperKey');
const Robot = require('./controllers/Robot');

var app = express();
var jsonParser = bodyParser.json()
app.use(cors())

//disperse master order
app.post("/disperseOrder", jsonParser, verifyAuthToken, function (req, res) {
    const { inPair, outPair, sl, enterprice, gain, clientsQueue } = req.body
    OrderKeepers.disperseOrder(inPair, outPair, sl, enterprice, gain, clientsQueue ).then((signalID)=>{
        res.send({success: 1, signalID})
    }, message=>{
        res.send({success: 0, message})
    })
})

//para regresar los bots prendidos

app.get("/getRobots", jsonParser, verifyAuthToken, async (req, res) => {
    try {
        let robots = await OrderKeepers.returnRobots()
        res.send({success: 1, robots})
    } catch (error) {
        res.send({success : 0, message : error.message ? error.message : error})
    }
})

//editar ordenes de compra
app.post("/editBuyOrders", jsonParser, verifyAuthToken, async (req, res) => {
    const {enterprice, symbol, signalID, tradList} = req.body
    let response = await OrderKeepers.verifyNewEnterPrice(signalID, symbol, enterprice)
    if(response.success === 0 ){
        res.send(response)
    } else {
        console.log("si pase los impedimentos")
        await OrderKeepers.editBuyOrders(signalID, enterprice, symbol, tradList)
        res.send({success: 1})
    }
})

app.post("/editOCO", jsonParser, verifyAuthToken, async (req, res) => {
    const { sl, gain, symbol, signalID, tradList } = req.body
    let response = await OrderKeepers.verifyNewSellPoints(signalID, symbol, gain, sl)
    if(response.success === 0 ){
        res.send(response)
    } else {
        console.log("si pase los impedimentos")
        await OrderKeepers.editOCO(signalID, sl, gain, symbol, tradList)
        res.send({success: 1})
    }
})

app.post("/deleteOrders", jsonParser, verifyAuthToken, async (req, res) => {
    let { signalID, symbol, tradList, asset } = req.body
    let response = await OrderKeepers.deleteOrders(signalID, symbol, tradList, asset)
    res.send(response)
})

//funcion para hacer pruebas con de encolamiento
app.get("/prenderBotsdeTodos", verifyDeveloperKey, async (req, res) => {
    await Trading.prenderBotsdeTodos()
    res.send({success: 1})
})

app.post("/generateAnalytics", jsonParser, verifyAuthToken, async (req, res) => {
    const { clientsParticipating } = req.body
    let response = await Trading.updateAnalytics(clientsParticipating)
    res.send(response)
})

app.get("/getQueued", jsonParser, verifyAuthToken, async (req, res) => {
    const {ready, notReady} = await Trading.getQueued()
    res.send({success: 1, ready, notReady})
}) 

app.post("/getMainQueue", jsonParser, verifyAuthToken, async (req, res) => {
    const { queueAmount, inPair } = req.body
    let activeClusters = await Trading.getActiveClusters()
    console.log("de active clusters", activeClusters)
    const queue = await Trading.getMainQueue(queueAmount, activeClusters, inPair)
    res.send({success: 1, queue})
}) 

app.get("/signalsHistory", jsonParser, verifyAuthToken, function (req, res) {    
    OrderKeepers.signalsHistory().then((signals=>{
        res.send({success: 1, signals})
    }))
})

app.get("/activeSignals", jsonParser, verifyAuthToken, function (req, res) {    
    OrderKeepers.activeSignals().then((signals=>{
        res.send({success: 1, signals})
    }))
}) 

app.post("/buyAsset", jsonParser, verifyAuthToken, async  (req, res) =>{    
    const { asset, quantity, buyType } = req.body
    const response = await Trading.buyAsset(asset, quantity, buyType)
    res.send(response)
}) 

app.post("/getCriptoAccount", jsonParser, verifyAuthToken, async  (req, res) =>{    
    const { account } = req.body
    const response = await Trading.getCriptoAccount(account)
    res.send(response)
})

app.post("/deleteExchgOrder", jsonParser, verifyAuthToken, async function (req, res) {
    let { account, origClientOrderId, symbol, skip } = req.body
    const response = await Robot.deleteExhgOrder(account, origClientOrderId, symbol, skip)
    res.send(response)
})

app.post("/sellQuantity", jsonParser, verifyAuthToken, async function (req, res) {
    let { quantity, asset, account, skip } = req.body
    let response = await Robot.sellQuantity(quantity, asset, account, skip)
    res.send(response)
})

app.get("/getParticipants", jsonParser, verifyAuthToken, async function (req, res) {
    let response = await Trading.updateOrdersData()
    res.send(response)
}) 

app.post("/verificateSymbolBalance", jsonParser, verifyAuthToken, async function (req, res) {
    const { tradList } = req.body
    let response = await Trading.verificateSymbolBalance(tradList)
    res.send(response)
})

app.get("/clusterOcuppation", jsonParser, verifyAuthToken, async function (req, res) {
    let response = await Trading.clusterOcuppation()
    res.send(response)
}) 

app.post("/refreshSocketOrders", jsonParser, verifyAuthToken, async function (req, res) {
    const { account } = req.body
    let response = await Trading.refreshSocketOrders(account)
    res.send(response)
}) 

app.post("/reBornVPS", jsonParser, verifyAuthToken, async function (req, res) {
    const { account, clusterNumber } = req.body
    let response = await Trading.reBornVPS(account, clusterNumber)
    res.send(response)
})

app.post("/deleteVPS", jsonParser, verifyAuthToken, async function (req, res) {
    const { account } = req.body
    let response = await Trading.deleteVPS(account)
    res.send(response)
})

app.post("/regenerateSellOrder", jsonParser, verifyAuthToken, async function (req, res) {
    let { sellable, asset, account } = req.body
    const response = await Robot.regenerateSellOrder(sellable, asset, account)
    res.send(response)
})

app.get("/getVPSWOOrders", jsonParser, verifyAuthToken, async function (req, res) {
    const response = await Trading.getVPSWOOrders()
    res.send(response)
}) 

app.post("/recoverySocketID", jsonParser, verifyAuthToken, async function (req, res) {
    let { account } = req.body
    const response = await Trading.recoverySocketID(account)
    res.send(response)
}) 

app.post("/deleteSlot", jsonParser, verifyAuthToken, async function (req, res) {
    let { account } = req.body
    const response = await Trading.deleteSlot(account)
    res.send(response)
})

app.post("/sellDust", jsonParser, verifyAuthToken, async function (req, res) {
    let { account } = req.body
    const response = await Trading.sellDust(account)
    res.send(response)
}) 

app.post("/regenerateQuantities", jsonParser, verifyAuthToken, async function (req, res) {
    let { account, inPair, initialOrderID, orderID, orderType } = req.body
    const response = await Trading.regenerateQuantities(account, inPair, initialOrderID, orderID, orderType)
    res.send(response)
}) 

app.post("/moveToHistoric", jsonParser, verifyAuthToken, async function (req, res) {
    let { orderID, signalID } = req.body
    const response = await Robot.moveToHistoric(orderID, signalID)
    res.send(response)
}) 

app.post("/powerOffAccountApp", jsonParser, verifyAuthToken, async function (req, res) {
    let { account, comments } = req.body
    const response = await Trading.turnOnApp(account, false, comments)
    res.send(response)
}) 
module.exports = app