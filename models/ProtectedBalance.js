var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var ProtectedBalanceSchema = Schema({
    userID: String,
    balances: Array
}, {timestamps: true});

module.exports = mongoose.model('ProtectedBalances', ProtectedBalanceSchema);