var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var ClustersSchema = Schema({
    name: String,
    address: String,
    clusterNumber: Number,
    testAddress: String,
});

module.exports = mongoose.model('Cluster', ClustersSchema);