var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var QueueSchema = Schema({
    userID: String
});

module.exports = mongoose.model('Queue', QueueSchema);