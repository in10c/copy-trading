var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var SignalsSchema = Schema({
    kind: String, //spot or futures
    inPair: String,
    outPair: String,
    entryPrice: Number,
    stopLoss: Number,
    takeProfit: Number,
    type: String,
    registeredTime: Date,
    kindOfClose: String,
    active: Boolean
}, {timestamps: true});

module.exports = mongoose.model('Signal', SignalsSchema);