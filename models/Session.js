// ¡¡¡¡¡¡¡  has a copy on server !!!!!!!!!!!!!!!

var mongoose =  require('mongoose');
var Schema = mongoose.Schema;

var SessionSchema = Schema({
    userID: String,
    email: String,
    btcBalance: Number,
    btcPortion: Number,
    btcInOrders: Number,
    bnbBalance: Number,
    openOrders: Number,
    historicOrders: Number,
    limits: Number,
    totalBalanceWithOrders: Number,
    time: Date,
    type: String,
    comments: String,
    buyedBNB: Boolean
});

module.exports = mongoose.model('Session', SessionSchema);